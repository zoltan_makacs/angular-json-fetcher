export interface User {
  id: string;
  age: number;
  firstName: string;
  lastName: string;
  gender: string;
  company: string;
  email: string;
  phone: string;
  address: string;
}
