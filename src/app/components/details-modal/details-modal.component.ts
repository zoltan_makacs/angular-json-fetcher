import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

export interface FormData {
  name: string;
  label: string;
  type: string;
}

@Component({
  selector: 'app-details-modal',
  templateUrl: './details-modal.component.html',
  styleUrls: ['./details-modal.component.scss']
})

export class DetailsModalComponent implements OnInit {
  userForm!: FormGroup;
  formData: FormData[] = [
    {
      name: 'firstName',
      label: 'First Name',
      type: 'text'
    },
    {
      name: 'lastName',
      label: 'Last Name',
      type: 'text'
    },
    {
      name: 'age',
      label: 'Age',
      type: 'Number'
    },
    {
      name: 'gender',
      label: 'Gender',
      type: 'text'
    },
    {
      name: 'company',
      label: 'Company',
      type: 'text'
    },
    {
      name: 'email',
      label: 'Email',
      type: 'text'
    },
    {
      name: 'phone',
      label: 'Phone',
      type: 'text'
    },
    {
      name: 'address',
      label: 'Address',
      type: 'text'
    },
  ];
  formGroupObject: any = {};

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(): void {
    this.formData.forEach(item => {
      this.formGroupObject[item.name + 'Control'] = new FormControl(this.data.user[item.name]);
    });

    this.userForm = this.fb.group(this.formGroupObject);
  }

}
