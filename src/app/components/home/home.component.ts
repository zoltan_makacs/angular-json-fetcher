import { Observable, Subscription } from 'rxjs';
import { JsonServiceService } from './../../services/json-service.service';
import { DetailsModalComponent } from './../details-modal/details-modal.component';
import { MatTableDataSource } from '@angular/material/table';
import { User } from './../../models/user.model';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  private subs: Subscription[] = [];

  public displayedColumns: string[] = ['firstName', 'lastName', 'age', 'phone', 'actions'];
  public dataSource = new MatTableDataSource<User>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    public dialog: MatDialog,
    private _liveAnnouncer: LiveAnnouncer,
    private jsonService: JsonServiceService) { }

  ngOnInit(): void {
    this.subs.push(
      this.jsonService.getUsers().subscribe((res: User[]) => {
        this.dataSource.data = res;
      })
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openDetailsModal(user: User): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '800px';
    dialogConfig.maxWidth = '100%';
    dialogConfig.data = {
      user
    };

    const dialogRef = this.dialog.open(DetailsModalComponent, dialogConfig);

    this.subs.push(
      dialogRef.afterClosed().subscribe(res => {
        console.log('Dialog was closed with a value of: ' + res);
      })
    );
  }

  changeTableSort(sortState: Sort) :void {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  ngOnDestroy(): void {
      this.subs.forEach(sub => sub.unsubscribe());
  }

}
