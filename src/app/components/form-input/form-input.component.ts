import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, forwardRef, Input } from '@angular/core';
import { FormData } from '../details-modal/details-modal.component';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(
        () => FormInputComponent
      ),
      multi: true
    }
  ]
})
export class FormInputComponent implements ControlValueAccessor {
  @Input() formControl!: FormControl;
  @Input() inputData!: FormData;

  public value!: string | number;
  public changed!: (value: string | number) => void;
  public touched!: () => void;

  constructor() { }

  writeValue(value: string | number): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.changed = fn;
  }
  registerOnTouched(fn: any): void {
    this.touched = fn;
  }

  isNumber(value: string | number): boolean {
    return typeof value === 'number';
  }

}
