import { TestBed } from '@angular/core/testing';

import { JsonServiceService } from './services/json-service.service';

describe('JsonServiceService', () => {
  let service: JsonServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
